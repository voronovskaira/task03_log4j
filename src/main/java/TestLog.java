import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class TestLog {
    private static  Logger LOG = LogManager.getLogger(TestLog.class);
    public static void main(String[] args) {
        LOG.debug("I'm Debug");
        LOG.info("I'm INFO");
        LOG.warn("I'm WARNING");
        LOG.error("I'm ERROR");
        LOG.fatal("I'm FATAL");
    }
}


